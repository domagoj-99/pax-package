### Kako pruzeti osnovni paket:

#### Prvi način(ssh)
```
git clone --recursive git@gitlab.com:AIBG/pax-package.git
```

#### Drugi način(uz vaš gitlab password)
```
git clone --recursive https://gitlab.com/AIBG/pax-package.git
```

#### Treći način(download .zip fajla)
samo downloadajte .zip file, međutim, ako mislite korisiti c++ primjer bota upišite:
```
git submodule init
```

## Pravila igre:

Originalna verzija igre Game of life:

(Pravila kopirana s wikipedije, guglati "game of life")

##### 1.Any live cell with fewer than two live neighbours dies, as if caused by under-population.
##### 2.Any live cell with two or three live neighbours lives on to the next generation.
##### 3.Any live cell with more than three live neighbours dies, as if by over-population.
##### 4.Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

Ta četiri pravila vrijede i u našoj verziji igre osim što se broj 
susjeda čelije računa se kao broj prijateljskih aktivnih susjeda - broj neprijateljskih aktivnih susjeda.


Igri prisustvuju dva bota. Polje je veličine 24x24. 
Prije računanja novog stanja botovi dodaju na polje određeni broj akcija (aktiviraju one čelije koje žele).
Postoji limit na udaljenost aktiviranja novih čelija od strane botova. Botovi također nesmiju probati aktivirati polja na kojima su žive suparnikove čelije.


Rad botova:
Za izradu botova možete koristiti c++, python i javu (ovdje imate primjere, koji doduše nisu update-ani ali će biti uskoro)

### Json koji se šalje igračima u svakoj iteraciji(potezu) sadrži sljedeće elemente:

##### "field": 
array od 24 elementa kojem je svaka string duljine 24 (personalizirano je botovima tako da znak '#' označava prijateljsku čeliju, '0' neprijateljsku, a '.' neaktivnu čeliju)
##### "cellsRemaining": 
koliko možete čelija aktivirati u ovom potezu (neće preći maxCellCapacity)
##### "cellGainPerTurn": 
koliko čelija ćete dobiti svaki potez za aktiviranje
##### "maxCellCapacity": 
koliko čelija možete maksimalno imati skladišteno( npr. odlučite štediti čelije ovaj potez i imati više čelija za sljedeći)
##### "maxColonisationDistance": 
koliko daleko od neke prijateljske aktivne čelije možete aktvirati svoju (bit će 2, u normi beskonačno, što znači kvadrat 5x5 oko bilo koje aktivne prijateljske čelije)
##### "currIteration": 
trenutna iteracija(počinje od nule)
##### "maxGameIterations": 
uvijek 500
##### "timeGainPerTurn": 
koliko svaki potez dobijete vremena (bit će 200 ms)
##### "timeLeftForMove": 
koliko ovaj potez imate vremena (200ms, na početku imate više jer znamo da jvm-u treba neko vrijem da se "zagrije")

### Json koji šaljete igri:

lista čelija koje želite aktivirati s nazivom "cells"
npr:
{"cells": [[10, 20], [10, 18], [12, 15], [10, 16], [12, 20]]}


# Važno!!

Trenutno primjeri botova nisu update-ani:

### java bot krivo računa udaljenost(koristi manhattan umjesto infinity norme)
### python bot ne provjerava pokušava li aktivirati polje sa suparničkom živom čelijom(možda ima i isti problem s udaljenosti)
### c++ bot vjerojatno isto nešto od toga :D

Ubrzo nakon početka ćemo update-ati botove
